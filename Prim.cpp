#include <cstring>
#include <iostream>
#include "Grafo.h"
using namespace std;

#define INF 9999999

// number of vertices in grapj

void inicializar_matriz_enteros (int **matriz, int n) {
	int temp, x, y;
	x = 1; // Estas variables solo sirven de contador
	y = 1;// mas que nada un tema de estetica del programa
	cout << "Ingrese los elementos de la matriz:" << endl;
    for (int fila=0; fila< n; fila++) {
        for (int col=0; col< n; col++) {
			cout << "M[" << x << "][" << y << "] = ";
			cin >> temp;
            matriz[fila][col] = temp;
            y++;
        }
        y = 1;
        x++;
    }
}

int main(int argc, char **argv) {
    int V;
    int no_edge;  // number of edge

    V = atoi(argv[1]);
	
	if (V <= 2) { // En caso de que no se cumpla los requisitos
		cout << "Uso: \n./matriz n" << endl;
		return -1;
	}

    int **G;
	G = new int*[V];
	
	for(int i=0; i<V; i++){
		G[i] = new int[V];
	}

    inicializar_matriz_enteros(G,V);

    char names[V];
    int inicio = 97;
  
    for (int i=0; i<V; i++) {
        names[i] = inicio+i;
    }

    // create a array to track selected vertex
    // selected will become true otherwise false
    int selected[V];
  
  

    // set selected false initially
    memset(selected, false, sizeof(selected));

    // set number of edge to 0
    no_edge = 0;

     // the number of egde in minimum spanning tree will be
    // always less than (V -1), where V is number of vertices in
    //graph

    // choose 0th vertex and make it true
    selected[0] = true;

    int x;  //  row number
    int y;  //  col number

    // print for edge and weight
    cout << "Edge"
         << " : "
        << "Weight";
    cout << endl;

    while (no_edge < V - 1) {
        //For every vertex in the set S, find the all adjacent vertices
        // , calculate the distance from the vertex selected at step 1.
        // if the vertex is already in the set S, discard it otherwise
        //choose another vertex nearest to selected vertex  at step 1.

        int min = INF;
        x = 0;
        y = 0;

        for (int i = 0; i < V; i++) {
            if (selected[i]) {
                for (int j = 0; j < V; j++) {
                    if (!selected[j] && G[i][j]) {  // not in selected and there is an edge
                        if (min > G[i][j]) {
                            min = G[i][j];
                            x = i;
                            y = j;
                        }
                    }
                }
            }
        }
        //cout << names[x] << " " << x << " - " << y << " :  " << G[x][y];
        cout << names[x] << " - " << names[y] << " :  " << G[x][y];
        cout << endl;
        selected[y] = true;
        no_edge++;
    }

    Grafo *g = new Grafo();
	g->imprimir_grafo(G,names,V);

  return 0;
}