# Guia 7

**Versión 1.0**

Archivo README.md para guía numero 7 de Algoritmos y Estructuras de Datos

Profesor: Alejandro Mauricio Valdes Jimenez

Asignatura: Algoritmos y Estructuras de Datos

Autor: Benjamín Ignacio Fisher Pino

Fecha: 15/11/2021

---
## Resúmen del programa

Este programa fue hecho por Benjamín Fisher para encontrar un arbol abarcador de costo minimo dada una matriz nxn.

---
## Requerimientos

Para utilizar este programa se requiere un computador con sistema operativo Linux Ubuntu version 20.04 L, un compilador g++ version 9.3.0, la version 4.2.1 de make y el paquete graphviz. En caso de no tener el compilador de g++, make o graphviz y no saber como descargarlos, abajo se encuentra una pequeña guia de como descargarlos.

### Instalacion de g++

Para instalar g++ debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install g++"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para compilar codigos de c y c++.

### Instalacion de make

Para instalar make debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install make"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar un ejecutable del programa.

### Instalacion de graphviz

Para instalar graphviz debe ir a su terminal de Linux y escribir el siguiente comando.

"sudo apt install graphviz"

Luego debera introducir su contraseña de super usuario (usuario ROOT) y en caso que se le pregunte por su autorizacion poner si. Si siguio estos sencillos pasos su computadora ya estaria lista para poder generar grafos del arbol construido.

---
## Como instalarlo

Para descargar los archivos debe dirigirse al repositorio alocado en el siguiente enlace.

Link: https://gitlab.com/BenjaminFisherPino/GUIA_7.git

Tras ingresar al repositorio debe proceder a clonarlo ó a descargar los archivos manualmente, ambas opciones funcionan.Luego debe dirigirse a travez de su terminal a la ruta donde se encuentran los archivos descargados. Una vez ahi, usted debe ingresar a la carpeta  y escribir el siguiente comando en su terminal.

"make" y posteriormente "./proyecto N"

N corresponde a un numero entero mayor a 2, el cual designa el tamaño de nuestra matriz. Es vital la utilizacion de este parametro para el funcionamiento del programa. ;)

En caso de tener problemas asegurece de tener la version 9.3.0 de g++, la version 4.2.1 de make y graphviz instalado.

Puede revisar con "g++ --version" y "make --version" respectivamente.

En el caso de graphviz puede comprobar escribiendo en su terminal "man graphviz", en caso de que no aparezca informacion del paquete debe revisar el tutorial de instalacion denuevo.

Si siguio todos los pasos debiese estar listo para ejecutar el programa!

---
## Funcionamiento del programa

El programa nos permite encontrar el arbol abarcador de costo minimo de una matriz nxn dada por el usuario, para esto se utiliza el algoritmo de Prim. El codigo esta estructurado en base a un archivo principal (Main) llamado "Prim.cpp", este nos permite mantener el orden de ejecucion y compilacion del programa. Se utilizaron dos archivos mas, "Grafo.cpp" el cual nos permmite la generacion de un grafo que nos muestra informacion del arbol generado y "Grafo.h" que sirve de modelo para "Grafo.cpp". Tras iniciado el programa se le pedira al usuario llenar la matriz nxn para luego aplicar el algoritmo de prim, imprimir el costo minimo y mostrar el grafo en una pestaña externa.

---

## License & Copyright

Ⓒ Benjamín Fisher Pino, Universidad de Talca

→ Mail: bfisher20@alumnos.utalca.cl
