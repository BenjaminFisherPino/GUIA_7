prefix=/usr/local
CC = g++

CFLAGS = -g -Wall 
SRC = Prim.cpp Grafo.cpp 
OBJ = Prim.o Grafo.o
APP = proyecto

all: $(OBJ)
	$(CC) $(CFLAGS)-o $(APP) $(OBJ) 

clean:
	$(RM) $(OBJ) $(APP)

install: $(APP)
	install -m 0755 $(APP) $(prefix)/bin

uninstall: $(APP)
	$(RM) $(prefix)/bin/$(APP)
