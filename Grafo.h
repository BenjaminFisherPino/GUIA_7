#ifndef GRAFO_H
#define GRAFO_H

#define TRUE 0
#define FALSE 1

class Grafo {
    
    public:
		Grafo();
		void imprimir_grafo(int **matriz, char *vector, int n);
};
#endif
