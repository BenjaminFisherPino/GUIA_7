#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
using namespace std;

#include "Grafo.h"

Grafo::Grafo() {}

void Grafo::imprimir_grafo(int **matriz, char *vector, int N) {
	int i, j;
	FILE *fp;
  
	fp = fopen("grafo.txt", "w");
	fprintf(fp, "%s\n", "graph G {");
	fprintf(fp, "%s\n", "graph [rankdir=LR]");
	fprintf(fp, "%s\n", "node [style=filled fillcolor=yellow];");
  
	for (i=0; i<N; i++) {
		for (j=0; j<N; j++) {
		// evalua la diagonal principal.
			if (i < j) {
				if (matriz[i][j] > 0) {
					fprintf(fp, "%c%s%c [label=%d];\n", vector[i],"--", vector[j], matriz[i][j]);
				}
			}
		}
	}
  
	fprintf(fp, "%s\n", "}");
	fclose(fp);

	system("dot -Tpng -ografo.png grafo.txt");
	system("eog grafo.png &");
	}
